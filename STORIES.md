# What is this used for?
## Management Info (MI)

- How long will the charging take (to arrive at 80%);
- How long before the charging reaches 80%;
- How long before the charging reaches 100%;
- Prediction: whats the estimated life of this battery;
- Gathering charging info. to support the life of Thinkpad T495s with a battery guarantee;
- Define the order of events once it reaches 80%. Use `/usr/bin/notify-send`;
- 

## Collection

### Everytime

1. readFile `/sys/class/power_supply/BAT0/uevent`;
2. Extract 1. into the/a DB/CSV file;
3. Mode of operation (MOP): See cmdargs modes of operation for what operation to perform when `batt-stat` starts;
4. MOP: `WARN`: The process by which 

## Control
1. Is there more than 1 BAT under `/sys/class/power_supply/`? Need to cater for > 1 battery;
2. What happens when there is no battery? In this case, as revealed by gitlab CI/CD, there is no `/sys/class/power_supply/`. Note: This maybe because its running in a VM. Done, see: [@ce4d8e87](https://gitlab.com/agander/batt-stat/-/commit/ce4d8e877ec676ffd88fb0e9c89e643b0d034110) 

## Operation
- Cron job/systemd timer task? Done, see: [#2](https://gitlab.com/agander/batt-stat/-/issues/2)
- What sort of DB? Straight sqlite or CSV. CSV could then be treated as a type of sqlite DB; Done, sqlite, see [@ac155c9f](https://gitlab.com/agander/batt-stat/-/commit/ac155c9fb418bc437df4bed35e13b630e20df8e2)
- Need to place a per user directory: ``~/.batt`. Done, maybe. DB is placed in `/usr/local/share/batt-stat`

## MOP
- `WARN`: The process where `batt-stat`reports on the estimated time of arrival to 80% if charging or to 20% if discharging. 
- DEFAULT/NO_MOP: Which MOP is the default action by `batt-stat` when it starts?


## Calculation

Need a ROC(rate of change calculation).

## DB Admin

Initially the Sqlite3 DB has 2 fields:

- ID - Int - the unix time (`date +%s`, `strftime('%s')`ecc.). Primary Key/uniq;
- RAW - Text - Sqlite3 text. Holds the raw data from `/sys/class/power_supply/BAT0/uevent`;
- 

## Available Fields
("POWER_SUPPLY_PRESENT","1"),
("POWER_SUPPLY_ENERGY_NOW","20000000"),
("POWER_SUPPLY_CYCLE_COUNT","29"),
("POWER_SUPPLY_CAPACITY","33"),
("POWER_SUPPLY_NAME","BAT0"),
("POWER_SUPPLY_CAPACITY_LEVEL","Normal"),
("POWER_SUPPLY_ENERGY_FULL_DESIGN","57000000"),
("POWER_SUPPLY_VOLTAGE_NOW","11999000"),
("POWER_SUPPLY_POWER_NOW","48295000"),
("POWER_SUPPLY_ENERGY_FULL","59230000"),
("POWER_SUPPLY_TECHNOLOGY","Li-poly"),
("POWER_SUPPLY_SERIAL_NUMBER"," 6349"),
("POWER_SUPPLY_STATUS","Charging"),
("POWER_SUPPLY_MANUFACTURER","LGC"),
("POWER_SUPPLY_MODEL_NAME","02DL013"),
("POWER_SUPPLY_VOLTAGE_MIN_DESIGN","11580000")

