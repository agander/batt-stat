# batt-stat
## A laptop battery stats collection and monitoring program.
### Battery Life Advice

2019: We are advised to keep Li-Po batteries between 20-80% charged.

My OS and GUI are Fedora and KDE. 

Admittedly my knowledge is limited but if we assume this to be an ‘ideal‘ situation then how does one achieve it?

i.e. So that it one starts a charge close to 20% and stops at around 80%.

The Fedora system is configured to warn at 20% and then close down services below that.

One doesnt want to:

1) leave it get to 100%;

2) allow it to get to 5%, certainly not 0%.

Ok, so thats the advice, then thats what `batt-stat` will do - strictly.

See [STORIES.md](https://gitlab.com/agander/batt-stat/-/blob/master/STORIES.md) for use cases.

See [SYSTEMD.md](https://gitlab.com/agander/batt-stat/-/blob/master/SYSTEMD.md) for instructions about how to setup a systemd timer task for `batt-stat`.

