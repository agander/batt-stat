{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, hpack, hspec, persistent-sqlite, split
      , sqlite-simple, stdenv, text, thyme, unordered-containers
      }:
      mkDerivation {
        pname = "batt-stat";
        version = "0.0.4";
        src = ./.;
        isLibrary = true;
        isExecutable = true;
        libraryHaskellDepends = [
          base persistent-sqlite split sqlite-simple text thyme
          unordered-containers
        ];
        libraryToolDepends = [ hpack ];
        executableHaskellDepends = [
          base persistent-sqlite split sqlite-simple text thyme
          unordered-containers
        ];
        testHaskellDepends = [
          base hspec persistent-sqlite split sqlite-simple text thyme
          unordered-containers
        ];
        prePatch = "hpack";
        homepage = "https://github.com/githubuser/batt-stat#readme";
        description = "Monitors the battery state between 20-80%";
        license = stdenv.lib.licenses.bsd3;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
