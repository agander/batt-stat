{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BlockArguments #-}

module Internal where

import           Protolude
import           Data.ByteString         (ByteString)
import qualified Data.ByteString         as B
import           Data.Text.Encoding      (encodeUtf8)
import qualified Data.ByteString.Lazy.Char8 as L8

import           Data.Text (Text)
import qualified Data.Text               as T
import qualified Data.Text.IO            as TIO
import           System.IO (  hGetLine, hClose, hPutStrLn
                            , stderr, stdout, hFlush)

import           Data.Typeable (Typeable)

import           Data.Version

import qualified Data.Map.Strict         as M
import           System.Process.Typed

import           Data.List               (sortOn)
import           Data.Maybe              (fromJust)

import           System.Log.Logger

import           Data.Csv

-- | sequence counter
yield_nums :: (Num a, Enum a) => a -> [a]
yield_nums maax = [1..maax]


-- T.unpack . T.justifyLeft 8 ' ' . T.justifyRight 6 ' ' $ T.pack (show n1)
merge_nums_and_cmds :: [Int] -> [[Char]] -> [Text]
merge_nums_and_cmds [] [] = []
merge_nums_and_cmds [] (_:_) = []
merge_nums_and_cmds [_] [] = []
merge_nums_and_cmds (_:_:_) [] = []
merge_nums_and_cmds (num:nums) (cmd:cmds) = T.pack ((justify_num num 5 8) ++ cmd) : merge_nums_and_cmds nums cmds

-- | The reason for justifying the idx number is for comparison purposes during
-- testing.
-- The 'history' cmd produces the following:
{-
$ history | head
    1  gvr incexp_14ott2014_16sett2018.csv

$ history | tail 
29989  history | head
-}
-- The history counter is 5 bytes followed by 2 spaces.

justify_num :: Show a => a -> Int -> Int -> [Char]
justify_num n r l = T.unpack . T.justifyLeft l ' ' .
                   T.justifyRight r ' ' 
                   $ T.pack (show n)

--progInfo :: MonadIO m => [Char] -> Version -> m ()
progInfo :: [Char] -> Version -> IO ()
progInfo prog version = do
  putStr $ ("Starting " :: [Char]) ++ prog ++ (" (version: " :: [Char])
  putStr $ showVersion version
  putStrLn (")" :: [Char])


process_typed_egs :: [Char] -> IO ()
process_typed_egs prog = do
  noticeM prog ": process_type_egs"
  {-
  -- Run a process, print its exit code
  runProcess "true" >>= print
  runProcess "false" >>= print

  -- Check that the exit code is a success
  runProcess_ "true"
  -- This will throw an exception: runProcess_ "false"

  -- Capture output and error
  (dateOut, dateErr) <- readProcess_ "date"
  print (dateOut, dateErr)

  -- Use shell commands
  (dateOut2, dateErr2) <- readProcess_ "date >&2"
  print (dateOut2, dateErr2)
  -}
  -- Interact with a process
  let catConfig = setStdin createPipe
                $ setStdout byteStringOutput
                $ proc "cat" ["/etc/hosts", "-", "/etc/group"]
  withProcessWait_ catConfig $ \p -> do
    hPutStr (getStdin p) ("HELLO\n" :: [Char])
    hPutStr (getStdin p) ("WORLD\n" :: [Char])
    hClose (getStdin p)

    atomically (getStdout p) >>= L8.putStrLn
    {-do $ atomically -}
      --out <- getStdout p
      --L8.putStr $ L8.take 10 out

-- | Record for batt-stat

