{-# LANGUAGE NoImplicitPrelude #-}

module Main where

import Protolude
import qualified Internal as I

main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
  --I.progInfo ""
  --interact putStrLn

  -- | get the stream of sqlite data from stdin
  rows <- getContents
  putStrLn "bye, Haskell!"

