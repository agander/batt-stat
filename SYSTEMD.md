# Instructions for creating a systemd timer & service.

### Once you have built `batt-stat` and are ready to deploy to collect stats, one way is to use systemd timer facilities.

1. `install` (using the system `install`, as I havent figured out the `cabal` method)

```
/bin/su -c "install -o gander -g gander --verbose $(stack exec which batt-stat) /usr/local/bin"
```
setting user/group appropriately, of course.

1. Create/edit: /etc/systemd/system/batt-stat.service

```
[Unit]
Description="A script to collect battery statistics"

[Service]
ExecStart=/usr/local/bin/batt-stat

WorkingDirectory=/usr/local/share/batt-stat

# Then, security is important so you generally want to not launch your service 
# with root privileges. User= and Group= enables you to set the user or group 
# name or UID/GID under which your application will be launched. For example:
User=gander
Group=gander
```

2. and: /etc/systemd/system/batt-stat.timer:

```
[Unit]
Description="Collects battery statistics"

[Timer]
OnBootSec=3min
OnUnitActiveSec=1min
Unit=batt-stat.service

[Install]
WantedBy=multi-user.target
```

Notes:
i) I chose `OnUnitActiveSec=1min` deliberately.
ii) The User & Group directives are so the job doesnt run as root.


3. To check the validity of the unit files do:

```
sudo /usr/bin/systemd-analyze verify /etc/systemd/system/batt-stat.*
```

4. Make a directory for the sqlite DB.
NB Not sure if this is the 'correct' place.

```
sudo mkdir -p /usr/local/share/batt-stat
```

and copy the sqlite DB (which magically got created when you 1st ran batt-stat).
to the directory chosen at 2.
```
sudo cp -p batt-stat.sqlite /usr/local/share/batt-stat/
```
30135  sudo /usr/bin/systemd-analyze verify /etc/systemd/system/batt-stat.*
30134  sqlite3 batt-stat.sqlite "SELECT id FROM batt_stat LIMIT 10;"

5. Set write perm to the directory holding the DB
```
sudo chmod 766 /usr/local/share/batt-stat/
```

6. and owner:group to the same as User/Group in batt-stat.service at 1.
```
sudo chown gander:gander /usr/local/share/batt-stat/
```

7. Now reload systemd: (I think so it can see all service/timer files)
```
sudo /usr/bin/systemctl daemon-reload
```

8. Attempt a start
```
sudo /usr/bin/systemctl start batt-stat.timer
```

and the watch the journalctl output:
```
journalctl --follow --all
```




