module Main where

--import Test.Hspec
import Test.HUnit
import qualified Data.Text as T
import Lib

--test1 = TestCase (assertEqual "for (foo 3)," (1,2) (foo 3))
{-test2 = TestCase do
  (x,y) <- partA 3
  assertEqual "for the first result of partA," 5 x
  b <- partB y
  assertBool ("(partB " ++ show y ++ ") failed") b
-}


test1 :: Test
test1 = TestCase (assertEqual "split_uevent_t: returns \"\" when given an empty list" 
  (split_uevent_t (T.pack "")) ([(T.pack "")]))

test2 :: Test
test2 = TestCase (assertEqual "split_uevent_t: returns [\"\",\"\"] when given a list containing only '='"
  (split_uevent_t (T.pack "=")) [T.pack "", T.pack ""])

test3 :: Test
test3 = TestCase (assertEqual "medio_di_tre: [1,2,3] == [2]" (medio_di_tre [1,2,3]) [2])

test4 :: Test
test4 = TestCase (assertEqual "medio_di_tre: [1,2,3,4] == [2,]" (medio_di_tre [1,2,3,4]) [2,3])

tests :: Test
tests = TestList [
    TestLabel "split_uevent_t 1" test1
  , TestLabel "split_uevent_t 2" test2
  , TestLabel "medio_di_tre 1" test3
  , TestLabel "medio_di_tre 2" test4
  ]

main :: IO ()
main = do
  _ <- runTestTT tests
  return ()

{-
hspec $ do
  describe "split_uevent_t" $ do
    it "returns \"\" when given an empty list" $ do
      split_uevent_t (T.pack "") `shouldBe` [(T.pack "")]

  describe "split_uevent_t" $ do
    it "returns [\"\",\"\"] when given a list containing only '='" $ do
      split_uevent_t (T.pack "=") `shouldBe` [T.pack "", T.pack ""]

  describe "medio_di_tre" $ do
    it "returns [] when given an empty list" $ do
      --tail [1,2,3] `shouldMatchList` [2,3]
      l <- medio_di_tre [1,2,3]
      return l `shouldReturn` [2]

  describe "medio_di_tre" $ do
    it "returns [] when given an empty list" $ do
      medio_di_tre [] `shouldBe` []
-}

