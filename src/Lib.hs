{-# LANGUAGE OverloadedStrings #-}

module Lib
(   split_uevent_t
  , make_tuple
  , get_current_datetime
  , have_power_supply
  , medio_di_tre
)
where

import           Data.List.Split
import qualified Data.Text as T
import           Data.Text.IO as TIO

import           Data.Time
import           System.IO (stderr, stdout) -- , hPutStrLn, hPutStr)
import           System.EasyFile (doesDirectoryExist)
import           System.Exit (exitFailure, exitSuccess)
import qualified Data.HashMap.Strict as HM

--
make_tuple :: [T.Text] -> (T.Text, T.Text)
make_tuple (t1:t2:_) = (t1, t2)

-- | function to split the BAT0/uevent list by '='
split_uevent_t :: T.Text -> [T.Text]
split_uevent_t = T.split (== '=')

-- | Get the current system time to be used as the Primary Key for the DB record.
get_current_datetime :: IO (Int)
get_current_datetime = do 
  myTime <- getZonedTime

  -- | Get the current time zone:
  curTZ <- getCurrentTimeZone
  let utcTime = zonedTimeToUTC myTime

  -- | works...
  --Prelude.putStr $ formatTime defaultTimeLocale "%Y%m%d%H%M.00" newMyTime
  --let str_time = formatTime defaultTimeLocale "%s" newMyTime
  let str_time = formatTime defaultTimeLocale "%s" utcTime
  return $ read str_time

  -- | /sys/class/power_supply/ not present on non battery machines
  -- | so abandon ship gracefully.
have_power_supply :: IO ()
have_power_supply = do
  have_ps <- doesDirectoryExist "/sys/class/power_supply"
  if have_ps
    then
      hPutStrLn stdout (T.pack "Found directory /sys/class/power_supply, so can collect battery stats")
    else
      hPutStrLn stderr (T.pack "No /sys/class/power_supply directory so cannot collect battery stats.") >> exitSuccess

{-
get_fiel :: T.Text -> (HM.singleton k v) -> Int
get_field key (HM.HashMap k v) = undefined TIO.putStr $ T.pack msg
  where
    let msg = "hm: lookup: " ++ key ++ ". "
    let pc = (read . T.unpack . fromJust) ( HM.lookup (T.pack key) hm) :: Int
  print pc
  -}

-- | medio_di_tre: generate a rolling average (medio in italiano)
-- | of every 3 items
--medio_di_tre :: (Num a, Integral a) => [a] -> [a]
medio_di_tre :: [Int] -> [Int]
medio_di_tre  [] = []
medio_di_tre (x:y:z:_) = [(x + y) + z `div` 3]

