{-# LANGUAGE UnicodeSyntax, OverloadedStrings #-}

module Main where

--import System.IO (hSetEncoding, stderr)
--import GHC.IO.Encoding

import qualified Data.Text as T
import           Data.Text.IO as TIO hiding (hPutStr, hPutStrLn)
import qualified Data.HashMap.Strict as HM
--import           Database.Sqlite
import           Lib

--import Control.Applicative
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow

import Data.Version
import System.IO (stdout, stderr, hPutStrLn, hPutStr, hFlush)

import Data.Aeson
import Data.Maybe (fromJust)
import Data.Char
--import Data.Text.Internal.Read

data TestField = TestField Int String deriving (Show)

instance FromRow TestField where
  fromRow = TestField <$> field <*> field

data BattStat = BattStat Int T.Text deriving (Show)

instance FromRow BattStat where
  fromRow = BattStat <$> field <*> field

version :: Version
version = makeVersion [0,1,4,0]

insomma :: Version -> String-> String
insomma vsn str = str ++ " " ++ showVersion vsn

dbg_output :: String -> Bool -> IO ()
dbg_output msg follow_on = do
  hPutStr stderr ">>> COMMENT: "
  case follow_on of
    True  -> hPutStr   stderr (msg ++ " ")
    False -> hPutStrLn stderr msg

main :: IO ()
main = do
  --setLocaleEncoding utf8
  --hSetEncoding stderr utf8
  {-
  Prelude.putStrLn "start..."
  --let lines = ["POWER_SUPPLY_NAME=BAT0","POWER_SUPPLY_STATUS=Discharging"]
  ps <- Prelude.readFile "/sys/class/power_supply/BAT0/uevent"
  let lynes = lines ps
  print $ Prelude.map split_uevent lynes
  Prelude.putStrLn "... fin"
  -}

  --D:TIO.putStrLn $ T.pack "start ..."
  hFlush stdout
  have_power_supply

  -- | Read from the 1st battery - assume 1 for now
  ps <- TIO.readFile "/sys/class/power_supply/BAT0/uevent"
  --D:--TIO.putStrLn $ T.pack "BAT0/uevent: "
  --D:--TIO.putStrLn ps
  let lynes = T.lines ps
  --let lines = ["POWER_SUPPLY_NAME=BAT0","POWER_SUPPLY_STATUS=Discharging"]
  --D:TIO.putStrLn $ T.pack "lynes: "
  --D:print lynes
  --mapM (TIO.putStr . split_uevent) T.lines
  --let unlynes = T.unlines ps
  let parts = map split_uevent_t lynes
  --D:TIO.putStrLn $ T.pack "parts: "
  --D:print parts

  let tup_list = map make_tuple parts
  --D:TIO.putStrLn $ T.pack "tup_list: "
  --D:print tup_list

  let hm = HM.fromList tup_list
  --D:TIO.putStrLn $ T.pack "hm: "
  --D:print hm
  --D:TIO.putStrLn $ T.pack "hm: lookup: POWER_SUPPLY_STATUS"
  --D:print $ HM.lookup (T.pack "POWER_SUPPLY_STATUS") hm

  let pc = (read . T.unpack . fromJust) ( HM.lookup (T.pack "POWER_SUPPLY_CAPACITY") hm) :: Int
  TIO.putStr $ T.pack "hm: lookup: POWER_SUPPLY_CAPACITY. "
  print pc
  --print $ HM.lookup (T.pack "POWER_SUPPLY_CAPACITY") hm
  --print $ read (fromJust ( HM.lookup (T.pack "POWER_SUPPLY_CAPACITY") hm)) :: Int
  --print $ map digitToInt (fromJust ( HM.lookup (T.pack "POWER_SUPPLY_CAPACITY") hm))
  --print $ concatMap digitToInt (T.unpack (fromJust ( HM.lookup (T.pack "POWER_SUPPLY_CAPACITY") hm)))
  --print $ map digitToInt (fromJust ( HM.lookup "POWER_SUPPLY_CAPACITY" hm))
  --print $ map ord (fromJust ( HM.lookup "POWER_SUPPLY_CAPACITY" hm))
  --print $ map ord (fromJust ( HM.lookup "POWER_SUPPLY_CAPACITY" hm))

  --D:TIO.putStr db_name

  -- | TODO: [76ace05] Need this to be found from an ini file or something.
  conn <- open "batt-stat.sqlite"
  --D:execute_ conn "CREATE TABLE IF NOT EXISTS test (id INTEGER PRIMARY KEY, str TEXT)"
  --D:execute conn "INSERT INTO test (str) VALUES (?)" (Only ("test string 2" :: String))
  --execute conn "INSERT INTO test (id, str) VALUES (?,?)" (TestField 13 "test string 3")
  --D:rowId <- lastInsertRowId conn
  --D:executeNamed conn "UPDATE test SET str = :str WHERE id = :id" [":str" := ("updated str" :: String), ":id" := rowId]
  {-
  r <- query_ conn "SELECT * from test;" :: IO [TestField]
  mapM_ print r
  execute conn "DELETE FROM test WHERE id = ?" (Only rowId)
   -- | used with simplest-sqlite
  smt <- prepare conn $ T.pack "select * from data;"
  row1 <- step smt >> columns smt
  row2 <- step smt >> columns smt
  print (row1, row2)
  finalize smt
  close conn
  -}

  execute_ conn "CREATE TABLE IF NOT EXISTS batt_stat (id INTEGER PRIMARY KEY, raw TEXT)"
  gct <- get_current_datetime
  execute conn "INSERT INTO batt_stat (id, raw) VALUES (?, ?)" (gct, ps)

  --D:qry <- query_ conn "SELECT * from batt_stat LIMIT 5;" :: IO [BattStat]
  --D:mapM_ print qry

  Database.SQLite.Simple.close conn
  --D:TIO.putStrLn $ T.pack "...fin"

db_name :: T.Text
db_name = T.pack "/home/gander/PROJ/haskell/batt-stat/batt-stat.sqlite"

--getInt :: String -> Int
--getInt (x:xs) = undefined
getInt = map ord

