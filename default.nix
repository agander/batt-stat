{ mkDerivation, base, hpack, hspec, persistent-sqlite, split
, sqlite-simple, stdenv, text, thyme, unordered-containers
}:
mkDerivation {
  pname = "batt-stat";
  version = "0.0.4";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base persistent-sqlite split sqlite-simple text thyme
    unordered-containers
  ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    base persistent-sqlite split sqlite-simple text thyme
    unordered-containers
  ];
  testHaskellDepends = [
    base hspec persistent-sqlite split sqlite-simple text thyme
    unordered-containers
  ];
  prePatch = "hpack";
  homepage = "https://github.com/githubuser/batt-stat#readme";
  description = "Monitors the battery state between 20-80%";
  license = stdenv.lib.licenses.bsd3;
}
